// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/SnakeGameGameModeBase.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnakeGameGameModeBase() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASnakeGameGameModeBase();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASnakeGameGameModeBase_NoRegister();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
// End Cross Module References
	void ASnakeGameGameModeBase::StaticRegisterNativesASnakeGameGameModeBase()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ASnakeGameGameModeBase);
	UClass* Z_Construct_UClass_ASnakeGameGameModeBase_NoRegister()
	{
		return ASnakeGameGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ASnakeGameGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASnakeGameGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "SnakeGameGameModeBase.h" },
		{ "ModuleRelativePath", "SnakeGameGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASnakeGameGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASnakeGameGameModeBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ASnakeGameGameModeBase_Statics::ClassParams = {
		&ASnakeGameGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ASnakeGameGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASnakeGameGameModeBase()
	{
		if (!Z_Registration_Info_UClass_ASnakeGameGameModeBase.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ASnakeGameGameModeBase.OuterSingleton, Z_Construct_UClass_ASnakeGameGameModeBase_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ASnakeGameGameModeBase.OuterSingleton;
	}
	template<> SNAKEGAME_API UClass* StaticClass<ASnakeGameGameModeBase>()
	{
		return ASnakeGameGameModeBase::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASnakeGameGameModeBase);
	ASnakeGameGameModeBase::~ASnakeGameGameModeBase() {}
	struct Z_CompiledInDeferFile_FID_project_Unreal5_SnakeGame_Source_SnakeGame_SnakeGameGameModeBase_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_project_Unreal5_SnakeGame_Source_SnakeGame_SnakeGameGameModeBase_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ASnakeGameGameModeBase, ASnakeGameGameModeBase::StaticClass, TEXT("ASnakeGameGameModeBase"), &Z_Registration_Info_UClass_ASnakeGameGameModeBase, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ASnakeGameGameModeBase), 828497470U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_project_Unreal5_SnakeGame_Source_SnakeGame_SnakeGameGameModeBase_h_3729061523(TEXT("/Script/SnakeGame"),
		Z_CompiledInDeferFile_FID_project_Unreal5_SnakeGame_Source_SnakeGame_SnakeGameGameModeBase_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_project_Unreal5_SnakeGame_Source_SnakeGame_SnakeGameGameModeBase_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
